# TODO can create instance of Checkout class
# TODO can add item price
# TODO can add an item
# TODO can calculate the current total
# TODO can add multiple items and get correct total
# TODO can add discount rules
# TODO can apply discout rules to the total
# TODO throw Exception if item added without a price

from Checkout import Checkout
import pytest

# test fixture to handle repeated Checkout class creation
@pytest.fixture()
def checkout():
    co = Checkout()
    co.addItemPrice("item1", 1)
    co.addItemPrice("item2", 2)
    return co

def test_CanCalculateTotal(checkout):
    checkout.addItem("item1")
    assert checkout.calculateTotal() == 1

def test_GetCorrectTotalWithMultipleItems(checkout):
    checkout.addItem("item1")
    checkout.addItem("item2")
    assert checkout.calculateTotal() == 3

def test_canAddDiscountRule(checkout):
    checkout.addDiscount("a", 3, 2)
