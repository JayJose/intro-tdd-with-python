# Unit Testing and Test-Driven Development with Python
by Richard Wells

![](images/tdd-cycle.png)


### Why do we unit test?
* Bugs hurt business!
* Software testing catches bugs before they reach application

### Levels of testing
* Unit testing - testing at the function Level
* Component testing - testing at the library and compiled binary level
* System testing - tests external interfaces
* Performance testing - testing done at sub-system and systems levels to verify timing and resource usages

### Overview of Unit Testing
* Testing individual functions
* Groups of tests can be combined into test suites for better organization
* Executes in development environment
* Execution should be automated

### Simple example
* 3 steps: Setup, Action, Assert

```python
import pytest

def str_len(some_string):
    return len(some_string)

def test_str_len():
    test_string = "1"               # Setup
    result = str_len(test_string)   # Action
    assert result == 1              # Assert
```
### Overview of Test Driven Development
* Process where **developer takes responsibility** for quality of code
* Unit tests are written **before** the production code

### Benefits of TDD
* Immediate feedback when changing code
* Writing tests serves as documentation

### TDD Workflow: Red, Green, Refactor
* Red - write a failing unit test
* Green - write just enough production code to make it pass test
* Refactor - clean up the test and production code

### Virtual Environment in Python 3
* venv is recommended over virtualenv
* built-in with Python3
* example usage in bash shell:
    ```bash
    python3 -m venv "tdd-intro"
    source tdd-intro/bin/activate
    pip3 install pytest
    deactivate
    ```
* can delete virtual environment by deleting directory

### Pycharm & Pytest
* built in PyTest functionality

### Overview of PyTest
* uses built-in assert statement
* pytest tests are function whose name begins with "test_"
* classes with tests should be named "Test%" and not have an init method
* file names of test modules should begin or end with "test_/_test"

```python
def test_dummyTest():
    assert 1 == 1
```
* bash command: pytest -v

### PyTest
* skip test using mark.skip decorator
```python
@pytest.mark.skip
```
